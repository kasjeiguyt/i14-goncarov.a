delete from items;

insert into items(id, barcode, code, name, price, stock, type, units) values
(1, '01010101', 'PR01', 'PREKĖ NR1', 1, 99, 'Prekė', 'Vnt.'),
(2, '02020202', 'PR02', 'PREKĖ NR2', 2, 99, 'Prekė', 'Vnt.'),
(3, '03030303', 'PR03', 'PREKĖ NR3', 3, 99, 'Prekė', 'Vnt.'),
(4, '04040404', 'PSL01', 'PASLAUGA NR1', 1, 99, 'Paslauga', 'Val.'),
(5, '05050505', 'PSL02', 'PASLAUGA NR2', 2, 99, 'Paslauga', 'Val.'),
(6, '06060606', 'PSL03', 'PASLAUGA NR3', 3, 99, 'Paslauga', 'Val.');


ALTER TABLE items AUTO_INCREMENT = 7;