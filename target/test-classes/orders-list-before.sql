delete from orders;

insert into orders(id, client_name, client_number, equipment_got, notes, status, user_id) values
(1, 'Anatolij', '865530715', 'LENOVO LEGION Y530', 'Sugadintas ekranas', 'Priimtas', 1),
(2, 'Anatolij', '865530715', 'HUAWEI P20 Pro', 'Sugadintas ekranas', 'Atiduotas', 2),
(3, 'Tomas', '860030715', 'LENOVO LEGION Y530', 'Sugadintas ekranas', 'Neapmokėtas', 1),
(4, 'Tomas', '860030715', 'HUAWEI P20 Pro', 'Sugadintas ekranas', 'Priimtas', 2);

ALTER TABLE orders AUTO_INCREMENT = 5;