delete from user_role;
delete from usr;

insert into usr(id, active, password, username) values
(1, true, 'admin', 'admin'),
(2, true, 'admin', 'user');

insert into user_role(user_id, roles) values
(1, 'DEVELOPER'),
(2, 'DEVELOPER');