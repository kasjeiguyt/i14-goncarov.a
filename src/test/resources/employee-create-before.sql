delete from employee;

insert into employee(id, duties, name, surname, username) values
(1, 'Developer', 'Anatolij', 'Gončarov', 'admin'),
(2, 'Director', 'Tomas', 'Balsys', 'user');

ALTER TABLE items AUTO_INCREMENT = 3;