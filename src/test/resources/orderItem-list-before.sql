delete from order_item;

insert into order_item(id, amount, barcode, code, item_id, name, order_id, price, type, units) values
(1, '2', '02020202', 'PR02', 2, 'PREKĖ NR2', 1, '2.99', 'Prekė', 'Vnt.'),
(2, '2', '05050505', 'PSL02', 5, 'PASLAUGA NR2', 1, '2.99', 'Paslauga', 'Val.');

ALTER TABLE order_item AUTO_INCREMENT = 3;