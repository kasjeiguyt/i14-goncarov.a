package com.example.jaja;

import com.example.jaja.controller.OrdersController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithUserDetails("admin")
@TestPropertySource("/application-test.properties")
@Sql(value = {"/create-user-before.sql", "/orders-list-before.sql", "/items-list-before.sql", "/orderItem-list-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/orders-list-after.sql", "/create-user-after.sql", "/items-list-after.sql", "/orderItem-list-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class OrdersControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private OrdersController ordersController;

    @Test
    public void ordersPageTest() throws Exception {
        this.mockMvc.perform(get("/orders"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("Atsijungti")))
                .andExpect(content().string(containsString("Naujas užsakymas")))
                .andExpect(content().string(containsString("Užsakymų paieška")))
                .andExpect(content().string(containsString("Užsakymų sąrašas")));
    }

    @Test
    public void ordersListTest() throws Exception {
        this.mockMvc.perform(get("/orders"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("//tbody[@id='orders-list']/tr").nodeCount(4));
    }

    @Test
    public void filterOrdersTest() throws Exception {
        this.mockMvc.perform(get("/orders").param("filterClientName", "Anatolij"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("//tbody[@id='orders-list']/tr").nodeCount(2))
                .andExpect(xpath("//tbody[@id='orders-list']/tr[@id=1]").exists())
                .andExpect(xpath("//tbody[@id='orders-list']/tr[@id=2]").exists());
    }

    @Test
    public void addOrderTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/orderAdd")
                .param("equipmentGot", "TEST EQUIPMENT")
                .param("clientName", "TEST")
                .param("clientNumber", "865555555")
                .param("notes", "TEST NOTES")
                .param("status", "TEST STATUS")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("//tbody[@id='orders-list']/tr").nodeCount(5))
                .andExpect(xpath("//tbody[@id='orders-list']/tr[@id=5]").exists())
                .andExpect(xpath("//tbody[@id='orders-list']/tr[@id=5]/td/span").string("TEST EQUIPMENT"));
    }

    @Test
    public void orderMoreInfoTest() throws Exception {
        this.mockMvc.perform(get("/orderMoreInfo").param("id", "1"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("Anatolij")))
                .andExpect(content().string(containsString("LENOVO LEGION Y530")))
                .andExpect(content().string(containsString("02020202")))
                .andExpect(content().string(containsString("05050505")));
    }

    @Test
    public void orderEditPageLoadingTest() throws Exception {
        this.mockMvc.perform(get("/orderEdit").param("id", "1"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("Anatolij")))
                .andExpect(content().string(containsString("LENOVO LEGION Y530")));
    }

    @Test
    public void orderEditSaveTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/orderEdit")
                .param("id", "1")
                .param("equipmentGot", "NEW TEST")
                .param("clientName", "TEST")
                .param("clientNumber", "865555555")
                .param("notes", "TEST NOTES")
                .param("status", "TEST STATUS")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("//tbody[@id='orders-list']/tr").nodeCount(4))
                .andExpect(xpath("//tbody[@id='orders-list']/tr[@id=1]").exists())
                .andExpect(xpath("//tbody[@id='orders-list']/tr[@id=1]/td/span").string("NEW TEST"));
    }
    @Test
    public void orderRemoveTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/orderRemove")
                .param("id", "1")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("//tbody[@id='orders-list']/tr").nodeCount(3))
                .andExpect(xpath("//tbody[@id='orders-list']/tr[@id=1]").doesNotExist());
    }

    @Test
    public void itemAddToOrderTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/orderItemAdd")
                .param("id", "1")
                .param("orderId", "1")
                .param("amount", "10")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated());

        this.mockMvc.perform(get("/orderMoreInfo").param("id", "1"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("Anatolij")))
                .andExpect(content().string(containsString("LENOVO LEGION Y530")))
                .andExpect(content().string(containsString("01010101")))
                .andExpect(content().string(containsString("02020202")));
    }

    @Test
    public void orderMoreInfoEditPageTest() throws Exception {
        this.mockMvc.perform(get("/orderMoreInfoEdit").param("id", "2"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("05050505")));
    }

    @Test
    public void orderMoreInfoEditSaveTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/orderMoreInfoEditSave")
                .param("id", "2")
                .param("orderId", "1")
                .param("itemId", "5")
                .param("code", "TEST CODE")
                .param("barcode", "00000000")
                .param("name", "TEST NAME")
                .param("price", "5")
                .param("type", "Paslauga")
                .param("units", "TEST UNITS")
                .param("amount", "5")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated());

        this.mockMvc.perform(get("/orderMoreInfo").param("id", "1"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("TEST CODE")))
                .andExpect(content().string(containsString("TEST UNITS")))
                .andExpect(content().string(containsString("00000000")))
                .andExpect(content().string(containsString("TEST NAME")));
    }

    @Test
    public void orderItemRemoveTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/orderMoreInfoRemove")
                .param("id", "2")
                .param("orderId", "1")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated());

        this.mockMvc.perform(get("/orderMoreInfo").param("id", "1"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("//*[@id='services-list']/tbody/tr").nodeCount(0));
    }


}