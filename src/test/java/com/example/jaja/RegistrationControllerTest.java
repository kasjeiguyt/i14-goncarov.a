package com.example.jaja;

import com.example.jaja.controller.MainController;
import com.example.jaja.controller.RegistrationController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.properties")
@Sql(value = {"/create-user-before.sql", "/access-code-create-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/access-code-create-after.sql", "/create-user-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class RegistrationControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private RegistrationController registrationController;

    @Autowired
    private MainController mainController;

    @Test
    public void contextLoads() throws Exception {
        this.mockMvc.perform(get("/registration"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Registruotis")))
                .andExpect(content().string(containsString("Prisijungti")));
    }

    @Test
    public void correctRegistrationTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/registration")
                .param("username", "tester")
                .param("password", "00000000")
                .param("passwordConfirm", "00000000")
                .param("accessCodeInput", "1010101")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/login"));
    }

    @Test
    public void badPasswordTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/registration")
                .param("username", "tester")
                .param("password", "00000000")
                .param("passwordConfirm", "00000001")
                .param("accessCodeInput", "1010101")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(content().string(containsString("Passwords are not equals")));
    }

    @Test
    public void userExistTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/registration")
                .param("username", "admin")
                .param("password", "00000000")
                .param("passwordConfirm", "00000000")
                .param("accessCodeInput", "1010101")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(content().string(containsString("User exists!")));
    }

    @Test
    public void badAccessCodeTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/registration")
                .param("username", "tester")
                .param("password", "00000000")
                .param("passwordConfirm", "00000000")
                .param("accessCodeInput", "00020200")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(content().string(containsString("Access code does not exist")));
    }

    @Test
    public void correctRegistrationWithLoginTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/registration")
                .param("username", "tester")
                .param("password", "00000000")
                .param("passwordConfirm", "00000000")
                .param("accessCodeInput", "1010101")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/login"));

        this.mockMvc.perform(formLogin().user("tester").password("00000000"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));
    }

}
