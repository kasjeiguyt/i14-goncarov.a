package com.example.jaja;

import com.example.jaja.controller.EmployeeController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithUserDetails("admin")
@TestPropertySource("/application-test.properties")
@Sql(value = {"/create-user-before.sql", "/employee-create-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/employee-create-after.sql", "/create-user-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class EmployeeControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EmployeeController EmployeeController;

    @Test
    public void employeePageTest() throws Exception {
        this.mockMvc.perform(get("/employee"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("Atsijungti")))
                .andExpect(content().string(containsString("Naujas darbuotojas")))
                .andExpect(content().string(containsString("Darbuotojų paieška")))
                .andExpect(content().string(containsString("Darbuotojų sąrašas")));
    }

    @Test
    public void addEmployeeTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/employee")
                .param("name", "TEST NAME")
                .param("surname", "TEST SURNAME")
                .param("duties", "TESTER")
                .param("username", "TESTER")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("TEST NAME")));
    }

    @Test
    public void employeeEditPageTest() throws Exception {
        this.mockMvc.perform(get("/employeeEdit").param("id", "1"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("Anatolij")));
    }

    @Test
    public void employeeEditSaveTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/employeeEditSave")
                .param("id", "1")
                .param("name", "TEST NAME")
                .param("surname", "TEST SURNAME")
                .param("duties", "TESTER")
                .param("username", "TESTER")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("TEST NAME")));
    }

    @Test
    public void employeeRemoveTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/employeeRemove")
                .param("id", "1")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("//*[@id='employee-list']/tbody/tr").nodeCount(1));

    }
}