package com.example.jaja;

import com.example.jaja.controller.EmployeeController;
import com.example.jaja.controller.ItemsController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithUserDetails("admin")
@TestPropertySource("/application-test.properties")
@Sql(value = {"/create-user-before.sql", "/items-list-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/items-list-after.sql", "/create-user-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class ItemsControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ItemsController ItemsController;

    @Test
    public void itemsPageTest() throws Exception {
        this.mockMvc.perform(get("/items"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("Atsijungti")))
                .andExpect(content().string(containsString("Nauja prėke")))
                .andExpect(content().string(containsString("Prėkių paieška")))
                .andExpect(content().string(containsString("Prėkių sąrašas")));
    }

    @Test
    public void addItemTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/items")
                .param("code", "TEST CODE")
                .param("barcode", "TEST BARCODE")
                .param("name", "TESTER")
                .param("units", "TEST")
                .param("price", "999")
                .param("type", "Paslauga")
                .param("stock", "999")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("TEST CODE")));
    }

    @Test
    public void itemsEditPageTest() throws Exception {
        this.mockMvc.perform(get("/itemEdit").param("id", "1"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("PR01")));
    }

    @Test
    public void itemsEditSaveTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/itemEditSave")
                .param("id", "1")
                .param("code", "TEST CODE")
                .param("barcode", "TEST BARCODE")
                .param("name", "TESTER")
                .param("units", "TEST")
                .param("price", "999")
                .param("type", "Paslauga")
                .param("stock", "999")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("TEST CODE")));
    }

    @Test
    public void employeeRemoveTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/itemRemove")
                .param("id", "1")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("//*[@id='items-list']/tbody/tr").nodeCount(5));
    }
}