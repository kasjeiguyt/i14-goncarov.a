package com.example.jaja;

import com.example.jaja.controller.AccessCodeController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithUserDetails("admin")
@TestPropertySource("/application-test.properties")
@Sql(value = {"/create-user-before.sql", "/access-code-create-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/access-code-create-after.sql", "/create-user-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class AccessCodeControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AccessCodeController accessCodeController;

    @Test
    public void accessCodePageTest() throws Exception {
        this.mockMvc.perform(get("/accesscode"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("Atsijungti")))
                .andExpect(content().string(containsString("Naujas prieigos raktas")))
                .andExpect(content().string(containsString("Prieigos raktų paieška")))
                .andExpect(content().string(containsString("Prieigos raktų sąrašas")));
    }

    @Test
    public void addAccessCodeTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/accesscode")
                .param("code", "11111111")
                .param("level", "Administratorius")
                .param("active", "True")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("11111111")));
    }

    @Test
    public void accessCodeEditPageTest() throws Exception {
        this.mockMvc.perform(get("/accessCodeEdit").param("id", "1"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("010101")));
    }

    @Test
    public void accessCodeEditSaveTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/accessCodeEditSave")
                .param("id", "1")
                .param("code", "00000000")
                .param("level", "Administratorius")
                .param("active", "True")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("00000000")));
    }

    @Test
    public void accessCodeRemoveTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/accessCodeRemove")
                .param("id", "1")
                .with(csrf());

        this.mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("//*[@id='access-code-list']/tbody/tr[2]").nodeCount(0));

    }
}