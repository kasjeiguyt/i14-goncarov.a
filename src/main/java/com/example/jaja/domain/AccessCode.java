package com.example.jaja.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class AccessCode {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id; //id

    //another data
    private String code;
    private String level;
    private Boolean active;

    //constructor

    public AccessCode() {
    }

    public AccessCode(String code, String level, Boolean active) {
        this.code = code;
        this.level = level;
        this.active = active;
    }

    public AccessCode(Integer id, String code, String level, Boolean active) {
        this.id = id;
        this.code = code;
        this.level = level;
        this.active = active;
    }

    public AccessCode(Integer id) {
        this.id = id;
    }



    //getters & setters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
