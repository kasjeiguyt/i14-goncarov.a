package com.example.jaja.domain;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    DEVELOPER;

    @Override
    public String getAuthority() {
        return name();
    }
}
