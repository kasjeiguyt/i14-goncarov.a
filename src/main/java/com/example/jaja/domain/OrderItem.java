package com.example.jaja.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class OrderItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer orderId;
    private Integer itemId;


    private String code;
    private String barcode;
    private String name;
    private String units;
    private Float price;
    private String type;

    private Float amount;


    public OrderItem() {
    }

    public OrderItem(Integer orderId, Integer itemId, String code, String barcode, String name, String units, Float price, String type, Float amount) {
        this.orderId = orderId;
        this.itemId = itemId;
        this.code = code;
        this.barcode = barcode;
        this.name = name;
        this.units = units;
        this.price = price;
        this.type = type;
        this.amount = amount;
    }

    public OrderItem(Integer id, Integer orderId, Integer itemId, String code, String barcode, String name, String units, Float price, String type, Float amount) {
        this.id = id;
        this.orderId = orderId;
        this.itemId = itemId;
        this.code = code;
        this.barcode = barcode;
        this.name = name;
        this.units = units;
        this.price = price;
        this.type = type;
        this.amount = amount;
    }

    public OrderItem(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }
}
