package com.example.jaja.domain;

import javax.persistence.*;

@Entity
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String notes;
    private String equipmentGot;
    private String clientName;
    private Integer clientNumber;
    private String status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User author;



    public Orders() {
    }

    public Orders(String notes, String equipmentGot, String clientName, Integer clientNumber, String status, User user) {
        this.notes = notes;
        this.equipmentGot = equipmentGot;
        this.clientName = clientName;
        this.clientNumber = clientNumber;
        this.status = status;
        this.author = user;
    }

    public Orders(Integer id, String notes, String equipmentGot, String clientName, Integer clientNumber, String status) {
        this.id = id;
        this.notes = notes;
        this.equipmentGot = equipmentGot;
        this.clientName = clientName;
        this.clientNumber = clientNumber;
        this.status = status;
    }

    public Orders(Integer id) {
        this.id = id;
    }

    public String getAuthorName() {
        return author != null ? author.getUsername() : "<none>";
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getEquipmentGot() {
        return equipmentGot;
    }

    public void setEquipmentGot(String equipmentGot) {
        this.equipmentGot = equipmentGot;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Integer getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
