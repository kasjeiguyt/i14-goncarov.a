package com.example.jaja.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Items {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id; //id

    //another data
    private String code;
    private String barcode;
    private String name;
    private String units;
    private Float price;
    private String type;
    private Float stock;

    public Items() {
    }

    public Items(String code, String barcode, String name, String units, Float price, String type, Float stock) {
        this.code = code;
        this.barcode = barcode;
        this.name = name;
        this.units = units;
        this.price = price;
        this.type = type;
        this.stock = stock;
    }

    public Items(Integer id) {
        this.id = id;
    }

    public Items(Integer id, String code, String barcode, String name, String units, Float price, String type, Float stock) {
        this.id = id;
        this.code = code;
        this.barcode = barcode;
        this.name = name;
        this.units = units;
        this.price = price;
        this.type = type;
        this.stock = stock;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Float getStock() {
        return stock;
    }

    public void setStock(Float stock) {
        this.stock = stock;
    }
}