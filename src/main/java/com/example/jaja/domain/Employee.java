package com.example.jaja.domain;

import javax.persistence.*;

@Entity
public class Employee {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id; //id

    //another data
    private String name;
    private String surname;
    private String duties;

    private String username;

    //constructor

    public Employee() {
    }

    public Employee(Integer id, String name, String surname, String duties) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.duties = duties;
    }

    public Employee(String name, String surname, String duties) {
        this.name = name;
        this.surname = surname;
        this.duties = duties;
    }

    public Employee(Integer id, String name, String surname, String duties, String username) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.duties = duties;
        this.username = username;
    }

    public Employee(String name, String surname, String duties, String username) {
        this.name = name;
        this.surname = surname;
        this.duties = duties;
        this.username = username;
    }

    public Employee(Integer id) {
        this.id = id;
    }




    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getDuties() {
        return duties;
    }

    public void setDuties(String duties) {
        this.duties = duties;
    }
}
