package com.example.jaja;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JajaApplication {

    public static void main(String[] args) {
        SpringApplication.run(JajaApplication.class, args);
    }

}
