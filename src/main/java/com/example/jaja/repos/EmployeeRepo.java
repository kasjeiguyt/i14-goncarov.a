package com.example.jaja.repos;

import com.example.jaja.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepo extends JpaRepository<Employee, Long> {

    List<Employee> findByNameContainingAndSurnameContainingAndDutiesContaining(String name, String surname, String duties);
    List<Employee> findByNameContainingAndDutiesContaining(String name, String duties);
    List<Employee> findByNameContainingAndSurnameContaining(String name, String surname);
    List<Employee> findByNameContaining (String name);


    List<Employee> findBySurnameContaining(String surname);
    List<Employee> findByDutiesContaining(String duties);
    List<Employee> findBySurnameContainingAndDutiesContaining(String surname, String duties);

    List<Employee> findById(Integer id);
}