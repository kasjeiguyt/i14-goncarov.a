package com.example.jaja.repos;

import com.example.jaja.domain.AccessCode;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccessCodeRepo extends CrudRepository<AccessCode, Long> {

    List<AccessCode> findByCodeContainingAndLevelAndActive(String code, String level, Boolean active);
    List<AccessCode> findByCodeContainingAndActive(String code, Boolean active);
    List<AccessCode> findByCodeContainingAndLevel(String code, String level);
    List<AccessCode> findByCodeContaining (String code);


    List<AccessCode> findByLevel(String level);
    List<AccessCode> findByActive(Boolean active);
    List<AccessCode> findByLevelAndActive(String level, Boolean active);

    List<AccessCode> findById(Integer id);

    AccessCode findByCode(String code);
}