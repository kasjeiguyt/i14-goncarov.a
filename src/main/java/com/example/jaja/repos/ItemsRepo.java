package com.example.jaja.repos;

import com.example.jaja.domain.Items;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface ItemsRepo extends CrudRepository<Items, Long> {
    List<Items> findByCodeContainingAndBarcodeContainingAndNameContainingAndType(String code, String barcode, String name, String  type);
    List<Items> findByCodeContainingAndBarcodeContainingAndNameContaining(String code, String barcode, String name);

    List<Items> findByCodeContainingAndBarcodeContainingAndType(String code, String barcode, String  type);
    List<Items> findByCodeContainingAndBarcodeContaining(String code, String barcode);

    List<Items> findByCodeContainingAndNameContainingAndType (String code, String name, String type);
    List<Items> findByCodeContainingAndNameContaining (String code, String name);

    List<Items> findByCodeContainingAndType (String code, String type);
    List<Items> findByCodeContaining (String code);

    List<Items> findByBarcodeContainingAndNameContainingAndType(String barcode, String name, String  type);
    List<Items> findByBarcodeContainingAndNameContaining(String barcode, String name);

    List<Items> findByBarcodeContainingAndType(String barcode, String  type);
    List<Items> findByBarcodeContaining(String barcode);

    List<Items> findByNameContainingAndType (String name, String type);
    List<Items> findByNameContaining (String name);

    List<Items> findByType (String type);

    List<Items> findById (Integer id);


}