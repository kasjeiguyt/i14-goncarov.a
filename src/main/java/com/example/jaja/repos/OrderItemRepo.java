package com.example.jaja.repos;

import com.example.jaja.domain.OrderItem;
import org.hibernate.criterion.Order;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderItemRepo extends CrudRepository<OrderItem, Long> {

    List<OrderItem> findByOrderIdAndType (Integer orderId, String type);
    List<OrderItem> findByOrderIdAndItemIdAndCodeAndBarcodeAndNameAndUnitsAndPriceAndType (Integer orderId, Integer itemId, String code, String barcode, String name, String units, Float price, String type);


    List<OrderItem> findById(Integer id);

}