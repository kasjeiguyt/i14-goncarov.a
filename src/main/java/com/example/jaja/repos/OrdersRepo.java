package com.example.jaja.repos;

import com.example.jaja.domain.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrdersRepo extends JpaRepository<Orders, Long> {
    List<Orders> findByEquipmentGotContainingAndClientNameContainingAndClientNumber (String equipmentGot, String clientName, Integer clientNumber);
    List<Orders> findByEquipmentGotContainingAndClientNameContaining (String equipmentGot, String clientName);

    List<Orders> findByEquipmentGotContainingAndClientNumber (String equipmentGot, Integer clientNumber);
    List<Orders> findByEquipmentGotContaining (String equipmentGot);

    List<Orders> findByClientNameContainingAndClientNumber (String clientName, Integer clientNumber);
    List<Orders> findByClientNameContaining (String clientName);

    List<Orders> findByClientNumber (Integer clientNumber);


    List<Orders> findById (Integer id);

}
