package com.example.jaja.controller;

import com.example.jaja.domain.AccessCode;
import com.example.jaja.repos.AccessCodeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class AccessCodeController {
    @Autowired
    private AccessCodeRepo accessCodeRepo;
    private String answerAccessCode;
    private Long allRecordsAccessCode;
    private Long foundRecordsAccessCode;

    @GetMapping("/accesscode")
    public String accessCode(Map<String, Object> model) {

        Iterable<AccessCode> accessCodes = accessCodeRepo.findAll();

        //geting size of iterable
        allRecordsAccessCode = accessCodes.spliterator().getExactSizeIfKnown();
        if (allRecordsAccessCode != 0) {
            answerAccessCode = allRecordsAccessCode + " of " + allRecordsAccessCode;
        } else {
            answerAccessCode = "No records found";
        }
        model.put("answerAccessCode", answerAccessCode);
        //end of geting size of iterable

        model.put("accessCodes", accessCodes);
        return "accessCode";
    }


    @PostMapping("accessCodeFilter")
    public String accessCodeFilter(@RequestParam String code,
                                   @RequestParam String level,                          //getting access level
                                   @RequestParam String activeString,                  //getting active status as string for null check
                                   Map<String, Object> model){
        Iterable<AccessCode> accessCodes;
        if(code != null && !code.isEmpty()) {
            if (level != null && !level.isEmpty()) {                                    //level check for null or empty
                if (activeString != null && !activeString.isEmpty()) {                         //level is and active is chosen
                    Boolean active = Boolean.parseBoolean(activeString);                   //converting string to boolean
                    accessCodes = accessCodeRepo.findByCodeContainingAndLevelAndActive(code, level, active);
                } else {                                                                //level is and active isn't chosen
                    accessCodes = accessCodeRepo.findByCodeContainingAndLevel(code, level);
                }
            } else {                                                                      //level isn't and active is chosen
                if (activeString != null && !activeString.isEmpty()) {
                    Boolean active = Boolean.parseBoolean(activeString);                   //converting string to boolean
                    accessCodes = accessCodeRepo.findByCodeContainingAndActive(code, active);
                } else {                                                                //level isn't and active isn't chosen
                    accessCodes = accessCodeRepo.findByCodeContaining(code);
                }
            }
        } else{
            if (level != null && !level.isEmpty()) {                                    //level check for null or empty
                if (activeString != null && !activeString.isEmpty()) {                         //level is and active is chosen
                    Boolean active = Boolean.parseBoolean(activeString);                   //converting string to boolean
                    accessCodes = accessCodeRepo.findByLevelAndActive(level, active);
                } else {                                                                //level is and active isn't chosen
                    accessCodes = accessCodeRepo.findByLevel(level);
                }
            } else {                                                                      //level isn't and active is chosen
                if (activeString != null && !activeString.isEmpty()) {
                    Boolean active = Boolean.parseBoolean(activeString);                   //converting string to boolean
                    accessCodes = accessCodeRepo.findByActive(active);
                } else {                                                                //level isn't and active isn't chosen
                    accessCodes = accessCodeRepo.findAll();
                }
            }
        }

        model.put("accessCodes", accessCodes);

        //geting size of iterable
        foundRecordsAccessCode = accessCodes.spliterator().getExactSizeIfKnown();
        if (foundRecordsAccessCode != 0) {
            answerAccessCode = foundRecordsAccessCode + " of " + allRecordsAccessCode;
        } else {
            answerAccessCode = "No records found";
        }
        model.put("answerAccessCode", answerAccessCode);
        //end of geting size of iterable

        return "accessCode";
    }

    @GetMapping("/accessCodeEdit")
    public String accessCodeEdit(
            @RequestParam Integer id,
            Map<String, Object> model) {

        Iterable<AccessCode> accessCodes = accessCodeRepo.findById(id);

        model.put("accessCodes", accessCodes);

        return "accessCodeEdit";
    }

    @PostMapping("/accesscode")
    public String add (@RequestParam String code,
                       @RequestParam String level,
                       @RequestParam Boolean active,
                       Map<String, Object> model) {

        AccessCode accessCode = new AccessCode(code, level, active);
        accessCodeRepo.save(accessCode);

        Iterable<AccessCode> accessCodes = accessCodeRepo.findAll();
        model.put("accessCodes", accessCodes);

        //geting size of iterable
        foundRecordsAccessCode = accessCodes.spliterator().getExactSizeIfKnown();
        if (foundRecordsAccessCode != 0) {
            answerAccessCode = foundRecordsAccessCode + " iš " + allRecordsAccessCode;
        } else {
            answerAccessCode = "Įrašų nerasta";
        }
        model.put("answerAccessCode", answerAccessCode);
        //end of geting size of iterable

        return "accesscode";
    }

    @PostMapping ("/accessCodeEditSave")
    public String orderEditSave(
            @RequestParam Integer id,
            @RequestParam String code,
            @RequestParam String level,
            @RequestParam Boolean active,
            Map<String, Object> model) {

        AccessCode accessCode = new AccessCode(id, code, level, active);
        accessCodeRepo.save(accessCode);

        Iterable<AccessCode> accessCodes = accessCodeRepo.findAll();
        model.put("accessCodes", accessCodes);

        //geting size of iterable
        foundRecordsAccessCode = accessCodes.spliterator().getExactSizeIfKnown();
        if (foundRecordsAccessCode != 0) {
            answerAccessCode = foundRecordsAccessCode + " iš " + allRecordsAccessCode;
        } else {
            answerAccessCode = "Įrašų nerasta";
        }
        model.put("answerAccessCode", answerAccessCode);
        //end of geting size of iterable



        return "accessCode";
    }

    @PostMapping ("/accessCodeRemove")
    public String accessCodeRemove(
            @RequestParam Integer id,
            Map<String, Object> model) {


        AccessCode accessCode = new AccessCode(id);
        accessCodeRepo.delete(accessCode);

        Iterable<AccessCode> accessCodes = accessCodeRepo.findAll();
        model.put("accessCodes", accessCodes);

        //geting size of iterable
        allRecordsAccessCode = accessCodes.spliterator().getExactSizeIfKnown();
        if (allRecordsAccessCode != 0) {
            answerAccessCode = allRecordsAccessCode+ " iš " + allRecordsAccessCode;
        } else {
            answerAccessCode = "Įrašų nerasta";
        }
        model.put("answerAccessCode", answerAccessCode);

        return "accessCode";
    }

}
