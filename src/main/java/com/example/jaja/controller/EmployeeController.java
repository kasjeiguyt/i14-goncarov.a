package com.example.jaja.controller;

import com.example.jaja.domain.Employee;
import com.example.jaja.domain.User;
import com.example.jaja.repos.EmployeeRepo;
import com.example.jaja.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class EmployeeController {
    @Autowired
    private EmployeeRepo employeeRepo;

    @Autowired
    private UserRepo userRepo;

    private Long foundRecordsEmployee;
    private Long allRecordsEmployee;
    private String answerEmployee;

    @GetMapping("/employee")
    public String employeeList(Map<String, Object> model) {

        Iterable<Employee> employees = employeeRepo.findAll();

        //getting size of iterable
        allRecordsEmployee = employees.spliterator().getExactSizeIfKnown();
        if (allRecordsEmployee != 0) {
            answerEmployee = allRecordsEmployee + " of " + allRecordsEmployee;
        } else {
            answerEmployee = "No records found";
        }
        model.put("answerEmployee", answerEmployee);
        //end of geting size of iterable

        model.put("employee", employees);
        return "employee";
    }

    //Employee add
    @PostMapping("/employee")
    public String addEmployee(@RequestParam String name,
                              @RequestParam String surname,
                              @RequestParam String duties,
                              @RequestParam String username,
                              Map<String, Object> model) {

        Employee employee = new Employee(name, surname, duties, username);

        employeeRepo.save(employee);

        Iterable<Employee> employees = employeeRepo.findAll();
        allRecordsEmployee = employees.spliterator().getExactSizeIfKnown();
        if (allRecordsEmployee != 0) {
            answerEmployee = allRecordsEmployee + " of " + allRecordsEmployee;
        } else {
            answerEmployee = "No records found";
        }


        model.put("employee", employees);
        model.put("answerEmployee", answerEmployee);
        return "employee";
    }

    @PostMapping("employeeFilter")
    public String accessCodeFilter(@RequestParam String name,
                                   @RequestParam String surname,
                                   @RequestParam String duties,
                                   Map<String, Object> model){


        Iterable<Employee> employees;
        if(name != null && !name.isEmpty()) {
            if (surname != null && !surname.isEmpty()) {                                    //level check for null or empty
                if (duties != null && !duties.isEmpty()) {                         //level is and active is chosen
                    employees = employeeRepo.findByNameContainingAndSurnameContainingAndDutiesContaining(name, surname, duties);
                } else {                                                                //level is and active isn't chosen
                    employees = employeeRepo.findByNameContainingAndSurnameContaining(name, surname);
                }
            } else {                                                                      //level isn't and active is chosen
                if (duties != null && !duties.isEmpty()) {
                    employees = employeeRepo.findByNameContainingAndDutiesContaining(name, duties);
                } else {                                                                //level isn't and active isn't chosen
                    employees = employeeRepo.findByNameContaining(name);
                }
            }
        } else{
            if (surname != null && !surname.isEmpty()) {                                    //level check for null or empty
                if (duties != null && !duties.isEmpty()) {                         //level is and active is chosen
                    employees = employeeRepo.findBySurnameContainingAndDutiesContaining(surname, duties);
                } else {                                                                //level is and active isn't chosen
                    employees = employeeRepo.findBySurnameContaining(surname);
                }
            } else {                                                                      //level isn't and active is chosen
                if (duties != null && !duties.isEmpty()) {
                    employees = employeeRepo.findByDutiesContaining(duties);
                } else {                                                                //level isn't and active isn't chosen
                    employees = employeeRepo.findAll();
                }
            }
        }

        model.put("employee", employees);

        //getting size of iterable
        foundRecordsEmployee = employees.spliterator().getExactSizeIfKnown();
        if (foundRecordsEmployee != 0) {
            answerEmployee = foundRecordsEmployee + " of " + allRecordsEmployee;
        } else {
            answerEmployee = "No records found";
        }
        model.put("answerEmployee", answerEmployee);
        //end of getting size of iterable

        return "employee";
    }

    @GetMapping("/employeeEdit")
    public String orderEdit(
            @RequestParam Integer id,
            Map<String, Object> model) {
        Iterable<User> users = userRepo.findAll();
        Iterable<Employee> employee = employeeRepo.findById(id);

        model.put("employeeUsername", users);
        model.put("employee", employee);

        return "employeeEdit";
    }

    @PostMapping ("/employeeEditSave")
    public String employeeEditSave(
            @RequestParam Integer id,
            @RequestParam String name,
            @RequestParam String surname,
            @RequestParam String duties,
            @RequestParam String username,
            Map<String, Object> model) {

        Employee employee = new Employee(id, name, surname, duties, username);
        employeeRepo.save(employee);

        Iterable<Employee> employees = employeeRepo.findAll();
        model.put("employee", employees);

        //getting size of iterable
        foundRecordsEmployee = employees.spliterator().getExactSizeIfKnown();
        if (foundRecordsEmployee != 0) {
            answerEmployee = foundRecordsEmployee + " iš " + allRecordsEmployee;
        } else {
            answerEmployee = "Įrašų nerasta";
        }
        model.put("answerEmployee", answerEmployee);
        //end of getting size of iterable

        return "employee";
    }

    @PostMapping ("/employeeRemove")
    public String employeeRemove(
            @RequestParam Integer id,
            Map<String, Object> model) {


        Employee employee = new Employee(id);
        employeeRepo.delete(employee);

        Iterable<Employee> employees = employeeRepo.findAll();


        //getting size of iterable
        allRecordsEmployee = employees.spliterator().getExactSizeIfKnown();
        if (allRecordsEmployee != 0) {
            answerEmployee = allRecordsEmployee + " iš " + allRecordsEmployee;
        } else {
            answerEmployee = "Įrašų nerasta";
        }
        model.put("answerEmployee", answerEmployee);
        model.put("employee", employees);

        return "employee";
    }
}
