package com.example.jaja.controller;

import com.example.jaja.domain.AccessCode;
import com.example.jaja.domain.Role;
import com.example.jaja.domain.User;
import com.example.jaja.repos.AccessCodeRepo;
import com.example.jaja.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Collections;
import java.util.Map;

@Controller
public class RegistrationController {
    @Autowired
    private UserRepo userRepo;

    @Autowired
    private AccessCodeRepo accessCodeRepo;

    @GetMapping("/registration")
    public String registration(){
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(User user, String password, String passwordConfirm, String accessCodeInput, Map<String, Object> model) {
        User userFromDB = userRepo.findByUsername(user.getUsername());
        AccessCode accessCode = accessCodeRepo.findByCode(accessCodeInput);

        if (userFromDB != null) {

            model.put("message", "User exists!");
            return "registration";
        }


        if (!password.equals(passwordConfirm)) {
           model.put("message", "Passwords are not equals");
            return "registration";
        }

        if (accessCode == null) {
            model.put("message", "Access code does not exist");
            return "registration";
        }

        user.setActive(true);
        user.setRoles(Collections.singleton(Role.DEVELOPER)); //spring cut for collection to get 1 data only
        userRepo.save(user);
        return "redirect:/login";
    }
}
