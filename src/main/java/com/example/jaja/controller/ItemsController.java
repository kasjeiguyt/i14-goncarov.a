package com.example.jaja.controller;

import com.example.jaja.domain.Items;
import com.example.jaja.repos.ItemsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class ItemsController {
    @Autowired
    private ItemsRepo itemsRepo;

    private String answerItems;
    private Long allRecordsItems;
    private Long foundRecordsItems;


    @GetMapping("/items")
    public String itemsList(Map<String, Object> model) {

        Iterable<Items> items = itemsRepo.findAll();

        //geting size of iterable
        allRecordsItems = items.spliterator().getExactSizeIfKnown();
        if (allRecordsItems != 0) {
            answerItems = allRecordsItems + " iš " + allRecordsItems;
        } else {
            answerItems = "Įrašų nerasta";
        }
        model.put("answerItems", answerItems);

        model.put("items", items);

        return "items";
    }

    @PostMapping("/items")
    public String add(
            @RequestParam String code,
            @RequestParam String barcode,
            @RequestParam String name,
            @RequestParam String units,
            @RequestParam Float price,
            @RequestParam String type,
            @RequestParam Float stock,
            Map<String, Object> model
    )
    {
        Items item = new Items(code, barcode, name, units, price, type, stock);

        itemsRepo.save(item);

        Iterable<Items> items = itemsRepo.findAll();

        //geting size of iterable
        allRecordsItems = items.spliterator().getExactSizeIfKnown();
        if (allRecordsItems != 0) {
            answerItems = allRecordsItems + " iš " + allRecordsItems;
        } else {
            answerItems = "Įrašų nerasta";
        }
        model.put("answerItems", answerItems);

        model.put("items", items);


        return "items";
    }

    @PostMapping("itemsFilter")
    public String itemsFilter(
            @RequestParam String code,
            @RequestParam String barcode,
            @RequestParam String name,
            @RequestParam String type,
            Map<String, Object> model
    )
    {
        Iterable<Items> items;

        if(code!=null && !code.isEmpty()) { //code is
            if(barcode!=null && !barcode.isEmpty()){ //barcode is
                if (name!=null && !name.isEmpty()) { //name is
                    if (type!=null && !type.isEmpty()){ //type is
                        items = itemsRepo.findByCodeContainingAndBarcodeContainingAndNameContainingAndType(code, barcode, name, type);
                    } else {
                        items = itemsRepo.findByCodeContainingAndBarcodeContainingAndNameContaining(code, barcode, name);
                    }
                } else {
                    if (type!=null && !type.isEmpty()){
                        items = itemsRepo.findByCodeContainingAndBarcodeContainingAndType(code, barcode, type);
                    } else {
                        items = itemsRepo.findByCodeContainingAndBarcodeContaining(code, barcode);
                    }
                }
            } else {
                if (name!=null && !name.isEmpty()) {
                    if (type!=null && !type.isEmpty()){
                        items = itemsRepo.findByCodeContainingAndNameContainingAndType(code, name, type);
                    } else {
                        items = itemsRepo.findByCodeContainingAndNameContaining(code, name);
                    }
                } else {
                    if (type!=null && !type.isEmpty()){
                        items = itemsRepo.findByCodeContainingAndType(code, type);
                    } else {
                        items = itemsRepo.findByCodeContaining(code);
                    }
                }
            }
        } else {
            if(barcode!=null && !barcode.isEmpty()){ //barcode is
                if (name!=null && !name.isEmpty()) { //name is
                    if (type!=null && !type.isEmpty()){ //type is
                        items = itemsRepo.findByBarcodeContainingAndNameContainingAndType(barcode, name, type);
                    } else {
                        items = itemsRepo.findByBarcodeContainingAndNameContaining(barcode, name);
                    }
                } else {
                    if (type!=null && !type.isEmpty()){
                        items = itemsRepo.findByBarcodeContainingAndType(barcode, type);
                    } else {
                        items = itemsRepo.findByBarcodeContaining(barcode);
                    }
                }
            } else {
                if (name!=null && !name.isEmpty()) {
                    if (type!=null && !type.isEmpty()){
                        items = itemsRepo.findByNameContainingAndType(name, type);
                    } else {
                        items = itemsRepo.findByNameContaining(name);
                    }
                } else {
                    if (type!=null && !type.isEmpty()){
                        items = itemsRepo.findByType(type);
                    } else {
                        items = itemsRepo.findAll();
                    }
                }
            }
        }
        model.put("items", items);

        //geting size of iterable
        foundRecordsItems = items.spliterator().getExactSizeIfKnown();
        if (foundRecordsItems != 0) {
            answerItems = foundRecordsItems + " iš " + allRecordsItems;
        } else {
            answerItems = "Įrašų nerasta";
        }
        model.put("answerItems", answerItems);
        //end of geting size of iterable

        return "items";
    }

    @GetMapping("itemEdit")
    public String itemEdit(
            @RequestParam Integer id,
            Map<String, Object> model) {

        Iterable<Items> items = itemsRepo.findById(id);
        model.put("items", items);

        return "itemEdit";
    }

    @PostMapping ("/itemEditSave")
    public String itemEditSave(
            @RequestParam Integer id,
            @RequestParam String code,
            @RequestParam String barcode,
            @RequestParam String name,
            @RequestParam String units,
            @RequestParam Float price,
            @RequestParam String type,
            @RequestParam Float stock,
            Map<String, Object> model) {

        Items item = new Items(id, code, barcode, name, units, price, type, stock);
        itemsRepo.save(item);

        Iterable<Items> items = itemsRepo.findById(id);
        model.put("items", items);

        //geting size of iterable
        foundRecordsItems = items.spliterator().getExactSizeIfKnown();
        if (foundRecordsItems != 0) {
            answerItems = foundRecordsItems + " of " + allRecordsItems;
        } else {
            answerItems = "No records found";
        }
        model.put("answerItems", answerItems);
        //end of geting size of iterable

        return "items";
    }

    @PostMapping ("/itemRemove")
    public String orderRemove(
            @RequestParam Integer id,
            Map<String, Object> model) {


        Items item = new Items(id);
        itemsRepo.delete(item);

        Iterable<Items> items = itemsRepo.findAll();
        model.put("items", items);

        //geting size of iterable
        allRecordsItems = items.spliterator().getExactSizeIfKnown();
        if (allRecordsItems != 0) {
            answerItems = allRecordsItems + " of " + allRecordsItems;
        } else {
            answerItems = "No records found";
        }
        model.put("answerItems", answerItems);

        return "items";
    }

}