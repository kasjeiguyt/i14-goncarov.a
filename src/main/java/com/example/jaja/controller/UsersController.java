package com.example.jaja.controller;

import com.example.jaja.domain.User;
import com.example.jaja.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
public class UsersController {
    @Autowired
    private UserRepo userRepo;

    private String answerUsers;
    private Long allRecordsUsers;

    @GetMapping("/users")
    public String users(Map<String, Object> model) {

        Iterable<User> users = userRepo.findAll();

        model.put("users", users);
//geting size of iterable
        allRecordsUsers = users.spliterator().getExactSizeIfKnown();
        if (allRecordsUsers != 0) {
            answerUsers = allRecordsUsers + " iš " + allRecordsUsers;
        } else {
            answerUsers = "Įrašų nerasta";
        }
        model.put("answerUsers", answerUsers);

        return "users";
    }



}
