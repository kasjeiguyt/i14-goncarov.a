package com.example.jaja.controller;

import com.example.jaja.domain.Items;
import com.example.jaja.domain.OrderItem;
import com.example.jaja.domain.Orders;
import com.example.jaja.domain.User;
import com.example.jaja.repos.ItemsRepo;
import com.example.jaja.repos.OrderItemRepo;
import com.example.jaja.repos.OrdersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class OrdersController {
    @Autowired
    private OrdersRepo ordersRepo;

    @Autowired
    private ItemsRepo itemsRepo;

    @Autowired
    private OrderItemRepo orderItemRepo;

    private String answerOrders;
    private Long allRecordsOrders;
    private Long foundRecordsOrders;

    //order list window
    @GetMapping("/orders") //filter and main page (orders)
    public String orders(@RequestParam(required = false, defaultValue = "") String filterEquipmentGot,
                         @RequestParam(required = false, defaultValue = "") String filterClientName,
                         @RequestParam(required = false, defaultValue = "") String filterClientNumberString,
        Map<String, Object> model)
        {
            Iterable<Orders> orders;
            Integer clientNumber = null;
            if(filterEquipmentGot != null && !filterEquipmentGot.isEmpty()) {
                if(filterClientName != null && !filterClientName.isEmpty()) {
                    if(filterClientNumberString != null && !filterClientNumberString.isEmpty()) {
                        clientNumber = Integer.parseInt(filterClientNumberString);
                        orders = ordersRepo.findByEquipmentGotContainingAndClientNameContainingAndClientNumber(filterEquipmentGot, filterClientName, clientNumber);
                    } else {
                        orders = ordersRepo.findByEquipmentGotContainingAndClientNameContaining(filterEquipmentGot, filterClientName);
                    }
                } else {
                    if(filterClientNumberString != null && !filterClientNumberString.isEmpty()) {
                        clientNumber = Integer.parseInt(filterClientNumberString);
                        orders = ordersRepo.findByEquipmentGotContainingAndClientNumber(filterEquipmentGot, clientNumber);
                    } else {
                        orders = ordersRepo.findByEquipmentGotContaining(filterEquipmentGot);
                    }
                }
            } else {
                if(filterClientName != null && !filterClientName.isEmpty()) {
                    if(filterClientNumberString != null && !filterClientNumberString.isEmpty()) {
                        clientNumber = Integer.parseInt(filterClientNumberString);
                        orders = ordersRepo.findByClientNameContainingAndClientNumber(filterClientName, clientNumber);
                    } else {
                        orders = ordersRepo.findByClientNameContaining(filterClientName);
                    }
                } else {
                    if(filterClientNumberString != null && !filterClientNumberString.isEmpty()) {
                        clientNumber = Integer.parseInt(filterClientNumberString);
                        orders = ordersRepo.findByClientNumber(clientNumber);
                    } else {
                        orders = ordersRepo.findAll();
                    }
                }
            }
            model.put("orders", orders);

            //geting size of iterable
            foundRecordsOrders = orders.spliterator().getExactSizeIfKnown();
            if (foundRecordsOrders != 0) {
                answerOrders = foundRecordsOrders + " iš " + allRecordsOrders;
            } else {
                answerOrders = "Įrašų nerasta";
            }
            model.put("answerOrders", answerOrders);
            //end of geting size of iterable

            return "orders";
    }


    //orderAdd process
    @PostMapping("/orderAdd") //order add
    public String orderAdd(@AuthenticationPrincipal User user,
                            @RequestParam String equipmentGot,
                              @RequestParam String notes,
                              @RequestParam String clientName,
                              @RequestParam Integer clientNumber,
                              @RequestParam String status,
                              Map<String, Object> model) {

        Orders order = new Orders(notes, equipmentGot, clientName, clientNumber, status, user);
        ordersRepo.save(order);

        Iterable<Orders> orders = ordersRepo.findAll();

        //geting size of iterable
        allRecordsOrders = orders.spliterator().getExactSizeIfKnown();
        if (allRecordsOrders != 0) {
            answerOrders = allRecordsOrders + " iš " + allRecordsOrders;
        } else {
            answerOrders = "Įrašų nerasta";
        }
        model.put("answerOrders", answerOrders);
        //end of geting size of iterable

        model.put("orders", orders);

        return "orders";
    }



    @GetMapping("/orderMoreInfo") //order more info page
    public String orderMoreInfo(
            @RequestParam Integer id,
            Map<String, Object> model) {

        Iterable<Orders> orders = ordersRepo.findById(id);

        String type = "Prekė";
        Iterable<OrderItem> orderInfoItems = orderItemRepo.findByOrderIdAndType(id, type);
        model.put("orderInfoItems", orderInfoItems);

        type = "Paslauga";
        Iterable<OrderItem> orderInfoServices = orderItemRepo.findByOrderIdAndType(id, type);
        model.put("orderInfoServices", orderInfoServices);

        model.put("orders", orders);

        return "orderMoreInfo";
    }


    @GetMapping("/orderEdit") //order edit page
    public String orderEdit(
            @RequestParam Integer id,
            Map<String, Object> model) {

        Iterable<Orders> orders = ordersRepo.findById(id);

        Iterable<Items> items = itemsRepo.findAll();

        model.put("orderId", id);

        model.put("orders", orders);
        model.put("items", items);


        return "orderEdit";
    }

    @PostMapping ("/orderEditSave") //order update page with order details update
    public String orderEditSave(
            @RequestParam Integer id,
            @RequestParam String equipmentGot,
            @RequestParam String notes,
            @RequestParam String clientName,
            @RequestParam Integer clientNumber,
            @RequestParam String status,
            Map<String, Object> model) {

        Orders order = new Orders(id, notes, equipmentGot, clientName, clientNumber, status);
        ordersRepo.save(order);

        Iterable<Orders> orders = ordersRepo.findAll();
        model.put("orders", orders);

        //geting size of iterable
        foundRecordsOrders = orders.spliterator().getExactSizeIfKnown();
        if (foundRecordsOrders != 0) {
            answerOrders = foundRecordsOrders + " iš " + allRecordsOrders;
        } else {
            answerOrders = "Įrašų nerasta";
        }
        model.put("answerOrders", answerOrders);
        //end of geting size of iterable



        return "orders";
    }

    @PostMapping ("/orderRemove") //order remove
    public String orderRemove(
            @RequestParam Integer id,
            Map<String, Object> model) {


        Orders order = new Orders(id);
        ordersRepo.delete(order);

        Iterable<Orders> orders = ordersRepo.findAll();
        model.put("orders", orders);

        //geting size of iterable
        allRecordsOrders = orders.spliterator().getExactSizeIfKnown();
        if (allRecordsOrders != 0) {
            answerOrders = allRecordsOrders + " iš " + allRecordsOrders;
        } else {
            answerOrders = "Įrašų nerasta";
        }
        model.put("answerOrders", answerOrders);

        return "orders";
    }

    @PostMapping("/orderItemAdd") //items add to order
    public String orderItemAdd(
            @RequestParam Integer id,
            @RequestParam Integer orderId,
            @RequestParam Float amount) {

        Iterable<Items> items = itemsRepo.findById(id);
        List<Items> itemsR = new ArrayList<>();
        items.forEach(itemsR :: add);

        for (Items itemsResult : itemsR) {
            Integer itemId = itemsResult.getId();
            String code = itemsResult.getCode();
            String barcode = itemsResult.getBarcode();
            String name = itemsResult.getName();
            String units = itemsResult.getUnits();
            Float price = itemsResult.getPrice();
            String type = itemsResult.getType();

            Iterable<OrderItem> savedItems = orderItemRepo.findByOrderIdAndItemIdAndCodeAndBarcodeAndNameAndUnitsAndPriceAndType(orderId, itemId, code, barcode, name, units, price, type);

            if (savedItems.spliterator().getExactSizeIfKnown() > 0) {
                List<OrderItem> savedItemR = new ArrayList<>();
                savedItems.forEach(savedItemR :: add);

                for (OrderItem savedItemResult : savedItemR) {
                    Float amountR = savedItemResult.getAmount();
                    Integer lineId = savedItemResult.getId();
                    Float amountResult = amountR + amount;

                    OrderItem orderItem = new OrderItem(lineId, orderId, itemId, code, barcode, name, units, price, type, amountResult);
                    orderItemRepo.save(orderItem);
                }
            } else {
                OrderItem orderItem = new OrderItem(orderId, itemId, code, barcode, name, units, price, type, amount);
                orderItemRepo.save(orderItem);
            }
        }

        return "redirect:/orderEdit?id="+orderId;
    }

    @PostMapping("/orderItemAddFilter")
    public String orderItemAddFilter(
            @RequestParam Integer orderId,
            @RequestParam String filterCode,
            @RequestParam String filterBarcode,
            @RequestParam String filterName,
            @RequestParam String filterType,
            Map<String, Object> model
    )
    {
        Iterable<Items> items;

        if(filterCode!=null && !filterCode.isEmpty()) { //code is
            if(filterBarcode!=null && !filterBarcode.isEmpty()){ //barcode is
                if (filterName!=null && !filterName.isEmpty()) { //name is
                    if (filterType!=null && !filterType.isEmpty()){ //type is
                        items = itemsRepo.findByCodeContainingAndBarcodeContainingAndNameContainingAndType(filterCode, filterBarcode, filterName, filterType);
                    } else {
                        items = itemsRepo.findByCodeContainingAndBarcodeContainingAndNameContaining(filterCode, filterBarcode, filterName);
                    }
                } else {
                    if (filterType!=null && !filterType.isEmpty()){
                        items = itemsRepo.findByCodeContainingAndBarcodeContainingAndType(filterCode, filterBarcode, filterType);
                    } else {
                        items = itemsRepo.findByCodeContainingAndBarcodeContaining(filterCode, filterBarcode);
                    }
                }
            } else {
                if (filterName!=null && !filterName.isEmpty()) {
                    if (filterType!=null && !filterType.isEmpty()){
                        items = itemsRepo.findByCodeContainingAndNameContainingAndType(filterCode, filterName, filterType);
                    } else {
                        items = itemsRepo.findByCodeContainingAndNameContaining(filterCode, filterName);
                    }
                } else {
                    if (filterType!=null && !filterType.isEmpty()){
                        items = itemsRepo.findByCodeContainingAndType(filterCode, filterType);
                    } else {
                        items = itemsRepo.findByCodeContaining(filterCode);
                    }
                }
            }
        } else {
            if(filterBarcode!=null && !filterBarcode.isEmpty()){ //barcode is
                if (filterName!=null && !filterName.isEmpty()) { //name is
                    if (filterType!=null && !filterType.isEmpty()){ //type is
                        items = itemsRepo.findByBarcodeContainingAndNameContainingAndType(filterBarcode, filterName, filterType);
                    } else {
                        items = itemsRepo.findByBarcodeContainingAndNameContaining(filterBarcode, filterName);
                    }
                } else {
                    if (filterType!=null && !filterType.isEmpty()){
                        items = itemsRepo.findByBarcodeContainingAndType(filterBarcode, filterType);
                    } else {
                        items = itemsRepo.findByBarcodeContaining(filterBarcode);
                    }
                }
            } else {
                if (filterName!=null && !filterName.isEmpty()) {
                    if (filterType!=null && !filterType.isEmpty()){
                        items = itemsRepo.findByNameContainingAndType(filterName, filterType);
                    } else {
                        items = itemsRepo.findByNameContaining(filterName);
                    }
                } else {
                    if (filterType!=null && !filterType.isEmpty()){
                        items = itemsRepo.findByType(filterType);
                    } else {
                        items = itemsRepo.findAll();
                    }
                }
            }
        }

        Iterable<Orders> orders = ordersRepo.findById(orderId);
        model.put("orders", orders);
        model.put("orderId", orderId);
        model.put("items", items);





        return "orderEdit";
    }

    @GetMapping("/orderMoreInfoEdit")
    public String orderMoreInfoEdit(
            @RequestParam Integer id,
            Map<String, Object> model){

        Iterable<OrderItem> orderItem = orderItemRepo.findById(id);
        model.put("orderItemEdit", orderItem);
        return "orderMoreInfoEdit";
    }
    @PostMapping("/orderMoreInfoEditSave")
    public String orderMoreInfoEditSave(
            @RequestParam Integer id,
            @RequestParam Integer orderId,
            @RequestParam Integer itemId,
            @RequestParam String code,
            @RequestParam String barcode,
            @RequestParam String name,
            @RequestParam Float price,
            @RequestParam String type,
            @RequestParam String units,
            @RequestParam Float amount,
            Map<String, Object> model) {

        OrderItem orderItem = new OrderItem(id, orderId, itemId, code, barcode, name, units, price, type, amount);

        if (amount >0) {
            orderItemRepo.save(orderItem);
        } else {
            orderItemRepo.delete(orderItem);
        }

        Iterable<Orders> orders = ordersRepo.findById(orderId);

        type = "Prekė";
        Iterable<OrderItem> orderInfoItems = orderItemRepo.findByOrderIdAndType(orderId, type);
        model.put("orderInfoItems", orderInfoItems);

        type = "Paslauga";
        Iterable<OrderItem> orderInfoServices = orderItemRepo.findByOrderIdAndType(orderId, type);
        model.put("orderInfoServices", orderInfoServices);

        model.put("orders", orders);

        return "orderMoreInfo";
    }

    @PostMapping("/orderMoreInfoRemove")
    public String orderMoreInfoRemove(
            @RequestParam Integer id,
            @RequestParam Integer orderId,
            Map<String, Object> model) {

        OrderItem orderItem = new OrderItem(id);
        orderItemRepo.delete(orderItem);

        Iterable<Orders> orders = ordersRepo.findById(orderId);

        String type = "Prekė";
        Iterable<OrderItem> orderInfoItems = orderItemRepo.findByOrderIdAndType(orderId, type);
        model.put("orderInfoItems", orderInfoItems);

        type = "Paslauga";
        Iterable<OrderItem> orderInfoServices = orderItemRepo.findByOrderIdAndType(orderId, type);
        model.put("orderInfoServices", orderInfoServices);

        model.put("orders", orders);

        return  "orderMoreInfo";
    }

}
